use strict;
use warnings; 
#The script will asociate one of 100 recodings to one of 100 datasets and recode it.


my @reduced_states = qw(H S N A C P);

my @random_recodings;
open (RECODINGS, "<100_recodings.txt");  #### This has to be changed if the name of the file with the included 100 recodings changes.

while (<RECODINGS>)
{
    my $line= $_;
    chomp $line;
    push (@random_recodings, $line);
}

my @files_to_recode =<*phy.20_sub>;  ### This has to be changed.  extension files to recode (have to be Phylyp sequential.

foreach my $file (@files_to_recode)
{
    print "DEBUG $file\n";
}

my $countfiles=0;
foreach my $random_recoding (@random_recodings)
{
    chomp $random_recoding;
    chomp $files_to_recode[$countfiles];
    open (IN, "<$files_to_recode[$countfiles]");
    my $outfile = $files_to_recode[$countfiles] . ".random_recoded";
    open (OUT, ">$outfile");
    
    ### defines 6 arrais each representing a class in random recoding
    $random_recoding =~ s/\[//g;
    print "DEBUG: $random_recoding\n";
    my @current_recoding = split (/\]/, $random_recoding); 
    
    my @class0 = split (//, $current_recoding[0]);
    my @class1 = split (//, $current_recoding[1]);
    my @class2 = split (//, $current_recoding[2]);
    my @class3 = split (//, $current_recoding[3]);
    my @class4 = split (//, $current_recoding[4]);
    my @class5 = split (//, $current_recoding[5]);
    ###
    
    #### DATA TO RECODE IN MEMORY
    my %data; # to store the original dataset
    while (<IN>)
    {
	my $line = $_;
	chomp $line;
	if ($line =~ /^20/)  ### Note this is for our study and our data have 20 taxa needs changing if used with datasets with different number of taxa.
	{
	    next;
	}
	my @taxa_data = split (/\s+/, $line);  # this read the data.  It assume it is a simple rectangular matrix or taxa and characters if original data in nexus or phylyp format all formatting must be removed to keep only the matrix itself  
	$data{$taxa_data[0]} = $taxa_data[1];
    }
    ######
    
    
#extract species list and count species in dataset
    my @data_keys = keys(%data); # list of species in dataset
    print OUT "#SPECIALALPHABET\n";
    print OUT "20 30000 HSNACP\n";    #### Note this is for our datasets have to change if number of characters and taxa differ from 20 and 30000
    foreach my $species (@data_keys)
    {
	my @current_real_sequence= split (//, $data{$species});
	print OUT $species . "     ";
	LINE: foreach my $Aminoacid (@current_real_sequence)
	{
	        if ($Aminoacid eq "-")
		{
		    print OUT "-";
		    next;
		}
		    foreach my $state_in_recoding_class (@class0)
		    {
			if ($Aminoacid eq $state_in_recoding_class)
			{
			    print OUT $reduced_states[0];
			    next;
			}
		    }
		    foreach my $state_in_recoding_class (@class1)
		    {
			if ($Aminoacid eq $state_in_recoding_class)
			{
			    print OUT $reduced_states[1];
			    next;
			}
		    }
		    foreach my $state_in_recoding_class (@class2)
		    {
			if ($Aminoacid eq $state_in_recoding_class)
			{
			    print OUT $reduced_states[2];
			    next;
			}
		    }
		    foreach my $state_in_recoding_class (@class3)
		    {
			if ($Aminoacid eq $state_in_recoding_class)
			{
			    print OUT $reduced_states[3];
			    next;
			}
		    }
		    foreach my $state_in_recoding_class (@class4)
		    {
			if ($Aminoacid eq $state_in_recoding_class)
			{
			    print OUT $reduced_states[4];
			    next;
			}
		    }
		    foreach my $state_in_recoding_class (@class5)
		    {
			if ($Aminoacid eq $state_in_recoding_class)
			{
			    print OUT $reduced_states[5];
			    next;
			}
		    } 
	}
	print OUT "\n";
    }
    $countfiles++;
} 
exit;
