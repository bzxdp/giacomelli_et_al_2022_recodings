use strict;
use warnings;
use List::Util qw/shuffle/;
### usage perl random_recoding_generator.pl 5000
### 5000 is the number of recodings generated

my $numer_recodings = $ARGV[0];

my @array = ("A","G","P","S","T","D","E","N","Q","H","K","R","F","Y","W","I","L","M","V","C");

my $firstclose = 4;
my $secondopen = 5;
my $secondclose = 8;
my $thirdopen = 9;
my $thirdclose = 11;
my $fourthopen = 12;
my $fourthclose = 14;
my $fifthopen = 15;
my $fifthclose = 18;
my $sixtopen = 19;
my $sixtclose = 19;

for (my $i=0; $i <= $numer_recodings; $i++)
{

    my @shuffled = shuffle(@array);

    print "[";

    my $counter =0;
foreach my $elemnt (@shuffled)
{
    if ($counter == $secondopen)
    {
	print "[";
    }
    if ($counter == $thirdopen)
    {
	print "[";
    }
    if ($counter == $fourthopen)
    {
        print "[";
    }
    if ($counter == $fifthopen)
    {
        print "[";
    }
    if ($counter == $sixtopen)
    {
	print "[";
    }

    print $shuffled[$counter];

    if ($counter == $firstclose)
    {
	print "]";
    }
    if ($counter == $secondclose)
    {
	print "]";
    }
    if ($counter == $thirdclose)
    {
	print "]";
    }
    if ($counter == $fourthclose)
    {
	print "]";
    }
    if ($counter == $fifthclose)
    {
	print "]";
    }
    if ($counter == $sixtclose)
    {
	print "]";
    }
    
    $counter++;
}

print  "\n";
}
