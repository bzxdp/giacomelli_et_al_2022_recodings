use strict;
use warnings; 
use Set::Intersection;

### usage: perl compare_recodings.pl random_recodings.txt recodings_of_target_similarity_to_dayhoff.txt 0.9
### where: random_recodings.txt is a list of recodings in the format [ABCDE][FGHI][JKL][MNO][PQRS][T], one per line.
### where: recodings_of_target_similarity_to_dayhoff.txt is the outfile with the list of randomised recodings.
### where: 0.9 is the required percent identity to Dayhoff-6 

my $infile= $ARGV[0];
my $outfile= $ARGV[1];
my $percent_identical= $ARGV[2];
  
open (OUT, ">All_similarities.txt");
open (OUT2, ">${outfile}" );
#### DAYHOFF6
my $D1 = ["A", "G", "P", "S", "T"]; 
my $D2 = ["D", "E", "N", "Q"]; 
my $D3 = ["H", "K", "R"]; 
my $D4 = ["F", "Y", "W"]; 
my $D5 = ["I", "L", "M", "V"]; 
my $D6 = ["C"];
#### 
my @dayhoff = ($D1, $D2, $D3, $D4, $D5, $D6);
#print "DEBUG @dayhoff\n";
my @random_recodings;
open (RECODINGS, "<${infile}");

while (<RECODINGS>)
{
    my $line= $_;
    chomp $line;
    push (@random_recodings, $line);
}

my $count_recodings=1;
foreach my $random_recoding (@random_recodings)
{
    my $current_recoding = $random_recoding;
    chomp $random_recoding;
    
    
### defines 6 arrays each representing a class (RC) in a random recoding.  These are ordered following the scheme of Dayhoff cardinality 5,4,3,3,1  
    $random_recoding =~ s/\[//g;
#    print "DEBUG: $random_recoding\n";
    my @current_recoding = split (/\]/, $random_recoding); 
    
    my $RC1 = [split (//, $current_recoding[0])];
    my $RC2 = [split (//, $current_recoding[1])];
    my $RC3 = [split (//, $current_recoding[2])];
    my $RC4 = [split (//, $current_recoding[3])];
    my $RC5 = [split (//, $current_recoding[4])];
    my $RC6 = [split (//, $current_recoding[5])];
    ###
    
    my @random_rec =($RC1, $RC2, $RC3, $RC4, $RC5, $RC6);
#  print ("debug @random_rec\n");
    
    my @AAs_in_same_group;
    
    
    
    my @intersection_C_IS_C = get_intersection($dayhoff[5], $random_rec[5]);
    if (@intersection_C_IS_C)
    {
# print "DEBUG C is C @intersection_C_IS_C\n";
	my $AA_together =scalar(@intersection_C_IS_C);
	push (@AAs_in_same_group, $AA_together);
    }
    foreach my $daycat (@dayhoff)
    {
	foreach my $rabdcat (@random_rec)
	{
	    my @intersection = get_intersection($daycat, $rabdcat);
	    #   print "DEBUG @intersection\n";
	    if (scalar(@intersection) > 1)
	    {
		my $AA_together =scalar(@intersection);
		push (@AAs_in_same_group, $AA_together); 
	    }
	    
	}
    }
    my $amount_similarity=0;
    for my $value  (@AAs_in_same_group)  
    {
	$amount_similarity+= $value;
    }
    my $similarity_percent = $amount_similarity/20;
# print "DEBUG similarity is: $amount_similarity, ($similarity_percent)\n";
    print OUT $current_recoding . "\t$similarity_percent\n";
    if ($similarity_percent >= $percent_identical) {print OUT2 $count_recodings . "\t" .  "$current_recoding  :::  $similarity_percent\n";}
    my $keepcount = ($count_recodings % 100000); 
    unless ($keepcount) {print "recoding $count_recodings done\n";}
    $count_recodings++;
}
exit;
